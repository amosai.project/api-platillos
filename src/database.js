import mongoose from 'mongoose';
import config from './config';

mongoose.connect(config.DB.URI, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

const connection = mongoose.connection;
connection.once('open', () => {
	console.log('>> Database is connected!');
});

connection.on('error', err => {
	console.log(err);
	process.exit(0);
});
