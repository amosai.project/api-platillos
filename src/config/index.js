export default {
	DB: {
		URI: process.env.MONGODB_URI || 'mongodb://localhost/api-platillos',
	},
};
