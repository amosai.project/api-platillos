import express, { json, urlencoded } from 'express';
import cors from 'cors';
import morgan from 'morgan';
import dotenv from 'dotenv';

const app = express();
dotenv.config();

app.set('port', process.env.PORT || 3000);

//Middlewares
if (process.env.NODE_ENV === 'development') {
	app.use(morgan('dev'));
}
app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cors());

export default app;
