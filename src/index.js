import '@babel/polyfill';
import app from './app';
import './database';

const port = app.get('port');

(async () => {
	app.listen(port);
	console.log(`>> Server on port: ${port}`);
})();
